<html>
<head>
<title>Project</title>
</head>
<h1>File list</h1>
<ul class="file_list">
  % for f in files:
    <li class="row file_list_item">
      <a class="col-md-3" href="entry/{{rev}}/{{f.path}}">
      <span class="glyphicon glyphicon-{{'folder-close' if f.gitObject.gitType == 'tree' else 'file'}}"></span>
      {{f.path}}
      </a>
    <span class="col-md-7">
      % if files[f]:
      <a class="text-muted" href="commit/{{str(files[f].sha)}}  ">
        {{files[f].first_line}}
      </a>
      % end
    </span>
    <span class="col-md-2 text-muted text-right timestamp"
            data-toggle="tooltip" data-placement="bottom"
            title="\${f.last_changed_time.strftime('%Y-%m-%d %H:%M:%S%z')}">
    </span>
    </li>
  % end
</ul>
</html>
