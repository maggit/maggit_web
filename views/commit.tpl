<html>
<head>
<title>Commit {{get('rev', str(commit.sha))}}</title>
</head>
<h1>Commit {{get('rev', str(commit.sha))}}</h1>
Commit by : {{commit.author}}
Message :
<pre>{{commit.message}}</pre>

Parents:
<ul>
% for c in commit.parents:
<li><a href="/commit/{{str(c.sha)}}">{{str(c.sha)}} {{c.first_line}}</a></li>
% end
</ul>

<ul>
% for name, what in diff.items():
<li>{{name}} : {{what}}</li>
% end
</ul>

</html>
