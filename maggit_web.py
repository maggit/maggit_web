#!/usr/bin/env python3

import bottle
from bottle import run, Bottle, view, abort, template
import maggit
import os

app = Bottle()
repo = maggit.Repo()

bottle.TEMPLATE_PATH = [os.path.join(os.path.dirname(__file__), 'views')]



@app.route('/')
def project():
    branch = repo.branches['master']

    entry_to_commit = branch.commit.get_first_appearances('', 1000)
    return template('project', rev='master', files={branch.commit.get_file(e):(c if c else None) for e,c in entry_to_commit.items()})

@app.route('/entry/<rev>/<path:path>')
def entry(rev, path):
    try:
        commit_sha = repo.get_full_sha(rev)
        commit = repo.get_commit(commit_sha)
    except KeyError:
        try:
            commit = repo.branches[rev].commit
        except KeyError:
            return "404"

    entry = commit.get_file(path)
    return template('entry', rev=rev, entry=entry)
    
@app.route('/blob/<sha>')
def blob(sha):
    blob = repo.get_blob(repo.get_full_sha(sha))
    return template('blob', blob=blob)
    
@app.route('/tree/<sha>')
def tree(sha):
    tree = repo.get_tree(repo.get_full_sha(sha))
    return template('tree', tree=tree)
    
@app.route('/commit/<rev>')
def commit(rev):
    try:
        commit_sha = repo.get_full_sha(rev)
        commit = repo.get_commit(commit_sha)
    except KeyError:
        try:
            commit = repo.branches[rev].commit
        except KeyError:
            return abort(404, "Cannot find rev %r"%rev)
    
    parent = commit.parents[0]
    diff = commit.tree.gen_full_diff_map(parent.tree)
    print(diff, str(commit.tree.sha), str(parent.tree.sha))
    
    return template('commit', rev=rev, commit=commit, diff=diff)
    
@app.route('/object/<sha>')
def object(sha):
    type_ = repo.db.object_type(repo.get_full_sha(sha))
    return {maggit.ObjectType.blob   : lambda: blob(sha),
     maggit.ObjectType.commit : lambda: commit(sha),
     maggit.ObjectType.tree   : lambda: tree(sha),
     maggit.ObjectType.tag    : lambda: tag(sha)
    }[type_]()

if __name__ == '__main__':
    run(app, host='localhost', port=8080, reloader=True)
