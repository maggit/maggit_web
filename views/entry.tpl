%from maggit import ObjectType
<html>
<head>
<title>Entry {{rev}} {{str(entry.path)}}</title>
</head>
<h1>Entry {{rev}} {{str(entry.path)}}</h1>
%if entry.path.parent != entry.path.parent.parent:
<a href="/entry/{{rev}}/{{str(entry.path.parent)}}">{{str(entry.path.parent)}}</a>
%else:
<a href="/commit/{{rev}}">{{rev}}</a>
%end

%if entry.gitObject.gitType == ObjectType.blob:
<pre>{{entry.gitObject.content}}</pre>
%elif entry.gitObject.gitType == ObjectType.tree:
<ul>
% for name in entry.gitObject.entries:
<li><a href="/entry/{{rev}}/{{str(entry.path/name)}}">{{name}}</a></li>
% end
</ul>
%end
</html>
