<html>
<head>
<title>Tree {{str(tree.sha)}}</title>
</head>
<h1>Tree {{str(tree.sha)}}</h1>
<ul>
% for name in tree.entries:
<li><a href="/{{tree.entries[name][1].gitType}}/{{str(tree.entries[name][1].sha)}}">{{name}}</a></li>
% end
</ul>
</html>
